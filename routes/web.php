<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('welcome', function () {
    return view('welcome');
});

//UserController
Route::get('/', 'UserController@adminLoginView');
Route::post('/adminLogin', 'UserController@adminLogin');
Route::get('/login', 'UserController@login');

//ListingController
Route::get('/allListing', 'ListingController@allListing');
Route::post('/addList', 'ListingController@addList');
Route::delete('/deleteList/{id}', 'ListingController@deleteList');
Route::get('/listing', 'ListingController@listing');
