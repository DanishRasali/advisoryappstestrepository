<?php session_start(); ?>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
		<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		
		<!------ Include the above in your HEAD tag ---------->
        <!-- Styles -->

    </head>
    <body>
		<div class="container">
         <div class="jumbotron">
            <h3>LISTINGS</h3>
         </div>
		 
		 
		 <div class="jumbotron">
			<form id="addListForm">
			  <input type="text" id="list_name" class="form-control" name="list_name" placeholder="List Name" style="margin-bottom: 1rem; border-radius: 0;">
			  <input type="number" step="0.1" id="distance" class="form-control" name="distance" placeholder="Distance" style="margin-bottom: 1rem; border-radius: 0;">
			  <input type="hidden" id="user_id" name="user_id" value="{{$_SESSION['user_id']}}">
			  <input type="submit" class="btn btn-primary" value="Add Listing">
			</form>
            
         </div>

         <div class="well">
			<table class="table table-dark table-striped">
				<thead>
					<tr>
						<th>ID</th>
						<th>List Name</th>
						<th>Distance (KM)</th>
						<th>User</th>
						<th>Options</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($listings as $l)
						<tr>
						<td>{{$l->id}}</td>
						<td>{{$l->list_name}}</td>
						<td>{{$l->distance}}</td>
						<td>{{$l->email}}</td>
						<td><button class="btn btn-danger" onclick="deleteList({{$l->id}})">Delete</button></td>
						</tr>
					@endforeach
				</tbody>
			</table>
         </div>
      </div>
	  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	  <script>
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		
		$(function() {

			$("#addListForm").submit(function(event){
			   event.preventDefault();
			   var formData = $(this).serialize();
				
			   $.ajax({
				  type: "POST",
				  url: 'addList',
				  data: formData,
				  dataType: "json",
				  success: function(list){
						Swal.fire({
						  icon: 'success',
						  title: 'Success',
						  text: list.message,
						}).then((result) => {
						  location.reload();
						});	
				  },
				  error: function() {
						Swal.fire({
						  icon: 'error',
						  title: 'Oops...',
						  text: 'An error occurred!',
						});
				  },
			   });

			}); 
	
		 });
		 
		 function deleteList(id){  
			$.ajax({
			  type: "DELETE",
			  url: 'deleteList/' + id,
			  dataType: "json",
			  success: function(list){
					Swal.fire({
					  icon: 'success',
					  title: 'Success',
					  text: list.message,
					}).then((result) => {
					  location.reload();
					});	
			  },
			  error: function() {
					Swal.fire({
					  icon: 'error',
					  title: 'Oops...',
					  text: 'An error occurred!',
					});
			  },
		   });
		}
	  </script>
    </body>
</html>
