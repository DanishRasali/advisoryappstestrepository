<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
	
	public function adminLoginView(){	
		return view('adminLogin');
	}
	
	public function adminLogin(){
		$email = (isset($_POST['email'])) ? $_POST['email'] : "";
		$password = (isset($_POST['password'])) ? $_POST['password'] : "";
		
		$user = DB::table('user')
					->where('email', $email)
					->where('type', 'a')
					->first();
					
		if($user){
			if (Hash::check($password, $user->encrypted_password)){
				session_start();
				$_SESSION['user_id'] = $user->id;
				$returnArray = array("id" => $user->id, "token" => $user->token, "status" => array("code" => 200,"message" => "Access Granted"));
			}else{
				$returnArray = array("status" => array("code" => 404,"message" => "User Not Found!"));
			}
		}else
			$returnArray = array("status" => array("code" => 404,"message" => "User Not Found!"));	
		
		echo json_encode($returnArray);
	}
	
	public function login(){
		$email = (isset($_GET['email'])) ? $_GET['email'] : "";
		$password = (isset($_GET['password'])) ? $_GET['password'] : "";
		
		$user = DB::table('user')
					->where('email', $email)
					->where('type', 'u')
					->first();
		if($user){
			if (Hash::check($password, $user->encrypted_password)){
				$returnArray = array("id" => $user->id, "token" => $user->token, "status" => array("code" => 200,"message" => "Access Granted"));
			}else{
				$returnArray = array("status" => array("code" => 404,"message" => "User Not Found!"));
			}
		}else
			$returnArray = array("status" => array("code" => 404,"message" => "User Not Found!"));	
		
		echo json_encode($returnArray);
	}
}
