<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ListingController extends Controller
{
    public function allListing(){
		$listings = DB::table('listing')
						->join('user', 'listing.user_id', '=', 'user.id')
						->select('listing.*', 'user.email')
						->get();
						
		return view('listings')
					->with('listings', $listings);
	}
	
	
	public function addList(){
		
		$list_name = (isset($_POST['list_name'])) ? $_POST['list_name'] : "";
		$distance = (isset($_POST['distance'])) ? $_POST['distance'] : "";
		$user_id = (isset($_POST['user_id'])) ? $_POST['user_id'] : "";
				
		DB::table('listing')->insert(
			[
				'list_name' => $list_name,
				'distance' => $distance,
				'user_id' => $user_id,
			]
		);
			
		echo json_encode(array("status" => 200, "message" => "List Added Successfully"));
	}

	public function deleteList($id){
		
		DB::table('listing')
			->where('id', $id)
			->delete();
			
		echo json_encode(array("status" => 200, "message" => "Delete Successful"));
	}
	
	public function listing(){
		
		$user_id = (isset($_GET['id'])) ? $_GET['id'] : "";
		$token = (isset($_GET['token'])) ? $_GET['token'] : "";
		
		$listings = DB::table('listing')
						->where('user_id', $user_id)
						->get();
		
		$returnArray = array("listing" => $listings, "status" => array("code" => 200,"message" => "Listing successfully retrieved"));
						
		echo json_encode($returnArray);
	}
}
