﻿# Host: 127.0.0.1  (Version 5.5.5-10.4.13-MariaDB)
# Date: 2020-08-04 18:35:07
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "listing"
#

DROP TABLE IF EXISTS `listing`;
CREATE TABLE `listing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_name` varchar(45) DEFAULT NULL,
  `distance` float(11,1) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

#
# Data for table "listing"
#

INSERT INTO `listing` VALUES (1,'Pantai Seafood Restaurant',1.9,1),(2,'Signature By The Hill @ the Roof',2.4,1),(3,'Cinnamon Coffee House',2.6,2),(4,'Village Park Restaurant',3.0,2),(5,'Ticklish Ribs & Wiches',4.2,1),(6,'myBurgerLab Sunway',7.7,1),(7,'the BULB COFFEE',2.4,2),(8,'PappaRich',2.5,1);

#
# Structure for table "user"
#

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `encrypted_password` varchar(255) DEFAULT NULL,
  `token` varchar(32) DEFAULT NULL,
  `type` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "user"
#

INSERT INTO `user` VALUES (1,'danishrasali@gmail.com','$2y$10$FY/C.mxr/iEm1N7suilOJOyavtUY6QmR7OSQBLVW4Mnyt//Y4qI5e','0298141a7e9818f68bdf25387b4a8f61','a'),(2,'user@user.user','$2y$10$FY/C.mxr/iEm1N7suilOJOyavtUY6QmR7OSQBLVW4Mnyt//Y4qI5e','47bd49deb6483b001a8d3567a1ef9a8f','u');
